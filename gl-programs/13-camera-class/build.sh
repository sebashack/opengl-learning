#!/bin/bash

rm -rf _build

LIB_FLAGS="-lglfw3 -lGL -lX11 -lpthread -lXrandr -lXi -ldl"
INCLUDES="-I ../glad/include -I ./include"
SOURCES="../glad/src/glad.c shader.cpp stb_image.cpp main.cpp"
OBJECTS="glad.o shader.o stb_image.o main.o"

g++ -std=c++17 ${INCLUDES} ${LIB_FLAGS} -c ${SOURCES}
g++ -std=c++17 ${OBJECTS} ${INCLUDES} ${LIB_FLAGS} -o main

mkdir _build && mv main _build
rm -f *.o
