#!/bin/bash

rm -rf _build

LIB_FLAGS="-lglfw3 -lGL -lX11 -lpthread -lXrandr -lXi -ldl"
GLAD_FLAGS="../glad/src/glad.c -I ../glad/include"

g++ main.cpp ${GLAD_FLAGS} ${LIB_FLAGS} -o main

mkdir _build && mv main _build
