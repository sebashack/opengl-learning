#!/bin/bash

sudo apt update
sudo apt install make cmake g++ libx11-dev libxi-dev libgl1-mesa-dev libglu1-mesa-dev \
                 libxrandr-dev libxext-dev libxcursor-dev libxinerama-dev libxi-dev \
                 pkg-config mesa-utils freeglut3-dev mesa-common-dev \
                 libglew-dev libglfw3-dev libglm-dev libao-dev libmpg123-dev \

GLFW_VERSION="3.3.8"
GLFW="glfw-${GLFW_VERSION}"

rm -rf ${GLFW}
rm -rf ${GLFW}.zip

wget https://github.com/glfw/glfw/releases/download/${GLFW_VERSION}/${GLFW}.zip

unzip ${GLFW}.zip

cd ${GLFW} && cmake . && make && sudo make install
